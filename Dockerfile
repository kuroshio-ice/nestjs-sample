FROM node:12.14.1-alpine
ENV APP_ROOT /app/

RUN apk update
RUN apk add bash vim

WORKDIR $APP_ROOT

COPY package*.json $APP_ROOT
RUN npm install

COPY . $APP_ROOT
